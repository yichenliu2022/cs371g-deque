// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"
#include <initializer_list>

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    // deque<int>,
    my_deque<int>,
    // deque<int, allocator<int>>
    my_deque<int, allocator<int>>
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(1024);
    const deque_type y(7);
    // ASSERT_TRUE(x.empty());
    // ASSERT_EQ(x[-1], 0);
    ASSERT_EQ(y.size(), 7);
}

// default constructor non-const
TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e);
}

// default constructor const
TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e);
}




// one argument constructor const
TYPED_TEST(DequeFixture, test4) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(10);
    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 10);
}

// one argument constructor non-const
TYPED_TEST(DequeFixture, test5) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(10);
    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 10);
}

// one argument constructor const
TYPED_TEST(DequeFixture, test6) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::const_iterator;
    const deque_type x(10);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(e, b + 10);
}

// one argument constructor non-const
TYPED_TEST(DequeFixture, test7) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(10);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(e, b + 10);
}



// fill constructor, const
TYPED_TEST(DequeFixture, test8) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(10, 5);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    while(b != e) {
        ASSERT_EQ(*b, 5);
        ++b;
    }
}

// fill constructor, non-const
TYPED_TEST(DequeFixture, test9) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(10, 5);
    iterator b = begin(x);
    iterator e = end(x);
    while(b != e) {
        ASSERT_EQ(*b, 5);
        ++b;
    }
}

// allocator constructor, const
TYPED_TEST(DequeFixture, test10) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    using allocator_type = typename deque_type::allocator_type;
    allocator_type a;
    const deque_type x(10, 5, a);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    while(b != e) {
        ASSERT_EQ(*b, 5);
        ++b;
    }
}

// allocator constructor, non-const
TYPED_TEST(DequeFixture, test11) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    using allocator_type = typename deque_type::allocator_type;
    allocator_type a;
    deque_type x(10, 5, a);
    iterator b = begin(x);
    iterator e = end(x);
    while(b != e) {
        ASSERT_EQ(*b, 5);
        ++b;
    }
}

//init-list constructor, const
TYPED_TEST(DequeFixture, test12) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    //ASSERT_TRUE(0);
    const deque_type y(x);
    //ASSERT_TRUE(0);
    initializer_list<int>::const_iterator b_x = x.begin();
    const_iterator b = begin(y);
    const_iterator e = end(y);

    while(b != e) {
        ASSERT_EQ(*b, *b_x);
        ++b;
        ++b_x;
    }
}

// copy assignment, non-const
TYPED_TEST(DequeFixture, test128) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(10, 5);
    deque_type y = x;
    x[0] = 1;
    ASSERT_EQ(y[0], 5);
}

// index operator, const
// modify to init-list constructor
TYPED_TEST(DequeFixture, test13) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    const deque_type y(x);
    ASSERT_EQ(y[3], 5);
}

// index operator, non-const
TYPED_TEST(DequeFixture, test14) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    ASSERT_EQ(y[3], 5);
}

// copy constructor, const
TYPED_TEST(DequeFixture, test15) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(10, 5);
    const deque_type y(x);
    ASSERT_EQ(x, y);
}

// copy constructor, non-const
TYPED_TEST(DequeFixture, test16) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(10, 5);
    deque_type y(x);
    ASSERT_EQ(x, y);
    y[0] = 3;
    ASSERT_EQ(x[0], 5);
}

// at, const
TYPED_TEST(DequeFixture, test17) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    const deque_type y (x);
    ASSERT_EQ(y.at(4), 6);
}

// at, non-const
TYPED_TEST(DequeFixture, test18) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y (x);
    ASSERT_EQ(y.at(4), 6);
}

// back, const
TYPED_TEST(DequeFixture, test19) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    const deque_type y (x);
    ASSERT_EQ(y.back(), 7);
}

// back, non-const
TYPED_TEST(DequeFixture, test20) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 8};
    deque_type y (x);
    ASSERT_EQ(y.back(), 8);
}

// begin, const
TYPED_TEST(DequeFixture, test21) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 8};
    const deque_type y (x);
    ASSERT_EQ(*y.begin(), 2);
}

// begin, non-const
TYPED_TEST(DequeFixture, test22) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 8};
    deque_type y (x);
    ASSERT_EQ(*y.begin(), 2);
}

// clear
TYPED_TEST(DequeFixture, test23) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(10);
    x.clear();
    ASSERT_EQ(x.size(), 0);
}

// empty const
TYPED_TEST(DequeFixture, test24) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(10, 3);
    ASSERT_TRUE(!x.empty());
}

// end const
TYPED_TEST(DequeFixture, test25) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(10, 3);
    const_iterator b = x.begin();
    for(int i = 0; i < 10; ++i) {
        ++b;
    }
    ASSERT_EQ(b, x.end());
}

// end non-const
TYPED_TEST(DequeFixture, test26) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    const initializer_list<int> x = {2, 3, 4};
    deque_type y(x);
    iterator b = y.begin();
    for(int i = 0; i < 3; ++i) {
        ++b;
    }
    ASSERT_EQ(b, y.end());
}

// erase
TYPED_TEST(DequeFixture, test27) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    iterator b = y.begin();
    b += 2;
    iterator i = y.erase(b);
    ASSERT_EQ(y[2], 5);
    ASSERT_EQ(y.size(), 5);
    ASSERT_EQ(*i, 5);
}

// front const
TYPED_TEST(DequeFixture, test28) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    const deque_type y(x);
    ASSERT_EQ(y.front(), 2);
}

// front non-const
TYPED_TEST(DequeFixture, test29) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {12, 3, 4, 5, 6, 7};
    deque_type y(x);
    ASSERT_EQ(y.front(), 12);
}


// insert
TYPED_TEST(DequeFixture, test30) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    const initializer_list<int> x = {1, 3, 4, 5, 6, 7};
    deque_type y(x);
    iterator b = y.begin();
    ++b;
    iterator i = y.insert(b, 2);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(*i, 2);
    ASSERT_EQ(y.size(), 7);
}

// pop back
TYPED_TEST(DequeFixture, test31) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    y.pop_back();
    ASSERT_EQ(y.size(), 5);
    ASSERT_EQ(y.back(), 6);
}

// pop back non-const
TYPED_TEST(DequeFixture, test32) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    y.pop_back();
    ASSERT_EQ(y.size(), 5);
    ASSERT_EQ(y.back(), 6);
}

// push back
TYPED_TEST(DequeFixture, test33) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    deque_type y(x);
    ASSERT_EQ(y.size(), 14);
    ASSERT_EQ(y.capacity(), 30);
    // ASSERT_EQ(y.capacity(), 100);
    y.push_back(8);
    y.push_back(9);
    y.push_back(10);
    ASSERT_EQ(y.size(), 17);
    ASSERT_EQ(y.back(), 10);
    ASSERT_EQ(y.capacity(), 30);
}

// push front
TYPED_TEST(DequeFixture, test34) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    y.push_front(1);
    y.push_front(2);
    y.push_front(3);
    y.push_front(4);
    ASSERT_EQ(y.size(), 10);
    ASSERT_EQ(y.front(), 4);
    ASSERT_EQ(y.back(), 7);
    ASSERT_EQ(y[2], 2);
}

// resize
TYPED_TEST(DequeFixture, test35) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    y.resize(0);
    ASSERT_EQ(y.size(), 0);
}



// resize
TYPED_TEST(DequeFixture, test36) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    y.resize(10, 5);
    ASSERT_EQ(y.size(), 10);
    ASSERT_EQ(y[9], 5);
}


// swap
TYPED_TEST(DequeFixture, test37) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    deque_type z(10, 0);
    initializer_list<int>::const_iterator b = x.begin();
    y.swap(z);
    for(size_t i = 0; i < z.size(); ++i) {
        ASSERT_EQ(z[i], *b);
        ++b;
    }
    for(size_t i = 0; i < y.size(); ++i) {
        ASSERT_EQ(y[i], 0);
    }
}

// pop front
TYPED_TEST(DequeFixture, test38) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    y.pop_front();
    ASSERT_EQ(y.size(), 5);
    ASSERT_EQ(y.back(), 7);
    ASSERT_EQ(y.front(), 3);
}

// resize
TYPED_TEST(DequeFixture, test39) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    y.resize(6);
    ASSERT_EQ(y.size(), 6);
    ASSERT_EQ(y.back(), 7);
}

// at
TYPED_TEST(DequeFixture, test40) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y(x);
    ASSERT_EQ(y.at(0), 2);
    ASSERT_EQ(y.at(3), 5);
}

// assignment
TYPED_TEST(DequeFixture, test41) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> x = {2, 3, 4, 5, 6, 7};
    deque_type y = x;
    ASSERT_EQ(y, x);
    ASSERT_EQ(y.at(3), 5);
}
