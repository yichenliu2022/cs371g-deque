// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// -------
// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...) {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    int i = 1;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;
            ++i;
        }
    }
    catch (...) {
        my_destroy(a, p, b);
        throw;
    }
}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * == operator
     * @param lhs left hand side deque
     * @param rhs right hand side deque
     * @return whether they are equal
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        if(lhs.size() != rhs.size()) {
            return false;
        }
        return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----------
    // operator <
    // ----------

    /**
     * < operator
     * @param lhs left hand side deque
     * @param rhs right hand side deque
     * @return whether lhs is less than rhs
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----
    // swap
    // ----

    /**
     * swap function
     * @param x lhs deque
     * @param y rhs deque
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type  = A;
    using value_type      = typename allocator_type::value_type;

    using size_type       = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer         = typename allocator_type::pointer;
    using const_pointer   = typename allocator_type::const_pointer;

    using reference       = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    // ----
    // data
    // ----

    size_t chunk_size = 10;

    allocator_type _a_in;
    allocator_type_2 _a_out;

    T** _b_out = nullptr;    //outer array, capacity
    T** _e_out = nullptr;
    size_t _b = 0;     //size, offset to the first element in deque
    size_t _e = 0;


private:
    // -----
    // valid
    // -----

    bool valid () const {
        return true;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
          * equal operator
          * @param lhs left hand side iterator, rhs right hand side iterator
          * @return whether they are equal
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (lhs.container == rhs.container) && (lhs.index == rhs.index);
        } 

        /**
          * not equal operator
          * @param lhs left hand side iterator, rhs right hand side iterator
          * @return whether they are not equal
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
          * + operator
          * @param lhs left hand side iterator, rhs value to add
          * @return whether they are equal
         */
        friend iterator operator + (iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * - operator
         * @param lhs left hand side iteartor
         * @param rhs right hand side iteartor
         * @return lhs - rhs
         */
        friend iterator operator - (iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::pointer;
        using reference         = typename my_deque::reference;

    private:
        // ----
        // data
        // ----
        my_deque<T, A>* container;
        size_t index;

    private:
        // -----
        // valid
        // -----

        bool valid () const {
            return (container != nullptr) && (index <= (*container).size());
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
          * pointer constructor
          * @param deque_given the deque being iterated on
          * @param index the place on deque that the iterator points to
         */
        iterator (my_deque<T, A>* deque_given, size_t i) {
            container = deque_given;
            index = i;
            assert(valid());
        }

        iterator             (const iterator&) = default;
        ~iterator            ()                = default;
        iterator& operator = (const iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * * operator
         * @return the actual data
         */
        //reference operator * () const {
        reference operator * () {
            return (*container)[index];
        } 

        // -----------
        // operator ->
        // -----------

        /**
         * -> opeartor
         * @return the pointer pointing at the same value as iterator
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * ++ operator, pre-increment
         * @return iterator after increment
         */
        iterator& operator ++ () {
            ++index;
            assert(valid());
            return *this;
        }

        /**
         * ++(int) operator, post_increment
         * @return iterator before increment
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * -- operator
         * @return iterator after decrement
         */
        iterator& operator -- () {
            --index;
            assert(valid());
            return *this;
        }

        /**
         * -- operator(int)
         * @return iterator before decrement
         */
        iterator operator -- (int) {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * += operator
         * @return iterator after increment
         */
        iterator& operator += (difference_type d) {
            index += d;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * -= operator
         * @return iterator after decrement
         */
        iterator& operator -= (difference_type d) {
            index -= d;
            assert(valid());
            return *this;
        }
    };

public:
    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * == operator
         * @param lhs left hand side interator rhs right hand side iterato
         * @return whether they are equal
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return  (lhs.container == rhs.container) && (lhs.index == rhs.index);
        } // fix

        /**
         * != operator
         * @param lhs left hand side interator rhs right hand side iterato
         * @return whether they are not equal
         */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * + operator
         * @param lhs left hand side interator rhs right hand side iterator
         * @return iterator after increment
         */
        friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * - operator
         * @param lhs left hand side interator rhs right hand side iterator
         * @return iterator after decrement
         */
        friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::const_pointer;
        using reference         = typename my_deque::const_reference;

    private:
        // ----
        // data
        // ----

        const my_deque<T, A>* container;
        size_t index;

    private:
        // -----
        // valid
        // -----

        bool valid () const {
            return (container != nullptr) && (index <= (*container).size());
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * reference constructor
         * @param p reference of data
         */
        const_iterator (const my_deque<T, A>* deque_given, size_t i) {
            container = deque_given;
            index = i;
            assert(valid());
        }

        const_iterator             (const const_iterator&) = default;
        ~const_iterator            ()                      = default;
        const_iterator& operator = (const const_iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * * operator
         * @return refernce to data
         */
        reference operator * () const {
            return (*container)[index];
        }           // fix

        // -----------
        // operator ->
        // -----------

        /**
         * -> opeartor
         * return pointer to reference
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * ++(int) operator
         * @return iterator before increment
         */
        const_iterator& operator ++ () {
            ++index;
            assert(valid());
            return *this;
        }

        /**
         * ++(int) operator
         * @return iterator after increment
         */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * --(int) operator
         * @return iterator after decrement
         */
        const_iterator& operator -- () {
            --index;
            assert(valid());
            return *this;
        }

        /**
         * --(int) operator
         * @return iterator before decrement
         */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * += operator
         * @return iterator after increment
         */
        const_iterator& operator += (difference_type d) {
            index += d;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * -= operator
         * @return iterator after decrement
         */
        const_iterator& operator -= (difference_type d) {
            index -= d;
            assert(valid());
            return *this;
        }
    };

public:
    // ------------
    // constructors
    // ------------

    my_deque ():
        _a_in(),
        _a_out() {
        _b_out = _a_out.allocate(3);
        _e_out = _b_out;
        _b_out[0] = _a_in.allocate(chunk_size);
        _b_out[1] = _a_in.allocate(chunk_size);
        _b_out[2] = _a_in.allocate(chunk_size);
        _b = chunk_size / 3 + chunk_size;
        _e = _b;
    }

    /**
     * my_deque size constructor
     * @param s size of deque
     */
    explicit my_deque (size_type s) :
        _a_in(),
        _a_out() {
           
        if (s != 0) {
            int actual_num = s / chunk_size;
            int chunk_to_allo = actual_num * 2 + 1;
            if(chunk_to_allo == 1){
                chunk_to_allo = 3;
            }
            _b_out = _a_out.allocate(chunk_to_allo);
            _e_out = _b_out + chunk_to_allo;

            for (int i = 0; i < chunk_to_allo; ++i) {
                _b_out[i] = _a_in.allocate(chunk_size);
            }

            _b = (chunk_to_allo * chunk_size) / 3;
            _e = _b + s;
            my_uninitialized_fill(_a_in, begin(), end(), value_type());
        }
        assert(valid());
    }


    /**
     * my_deque size constructor with filled in value
     * @param s size
     * @param v value to filled with
     */
    my_deque (size_type s, const_reference v) :
        _a_in(),
        _a_out() {
        if (s != 0) {
            int actual_num = s / chunk_size;
            int chunk_to_allo = actual_num * 2 + 1;
            if(chunk_to_allo == 1){
                chunk_to_allo = 3;
            }
            _b_out = _a_out.allocate(chunk_to_allo);
            _e_out = _b_out + chunk_to_allo;

            for (int i = 0; i < chunk_to_allo; ++i) {
                _b_out[i] = _a_in.allocate(chunk_size);
            }
            _b = (chunk_to_allo * chunk_size) / 3;
            _e = _b + s;
            my_uninitialized_fill(_a_in, begin(), end(), v);
        }
        assert(valid());
    }

    /**
     * my_deque size constructor with filled in value and user given allocator
     * @param s size
     * @param v value to filled with
     * @param a the user given allocator
     */
    my_deque (size_type s, const_reference v, const allocator_type& a) :
        _a_in(a),
        _a_out() {
        if (s != 0) {
            int actual_num = s / chunk_size;
            int chunk_to_allo = actual_num * 2 + 1;
            if(chunk_to_allo == 1){
                chunk_to_allo = 3;
            }
            _b_out = _a_out.allocate(chunk_to_allo);
            _e_out = _b_out + chunk_to_allo;

            for (int i = 0; i < chunk_to_allo; ++i) {
                _b_out[i] = _a_in.allocate(chunk_size);
            }
            _b = (chunk_to_allo * chunk_size) / 3;
            _e = _b + s;
            my_uninitialized_fill(_a_in, begin(), end(), v);
        }
        assert(valid());
    }

    /**
     * init-list constructor
     * @param rhs the init-list to construct with
     */
    my_deque (std::initializer_list<value_type> rhs) :
        _a_in(),
        _a_out() {
        auto s = std::distance(std::begin(rhs), std::end(rhs));
        if (s != 0) {
            int actual_num = s / chunk_size;
            int chunk_to_allo = actual_num * 2 + 1;
            if(chunk_to_allo == 1){
                chunk_to_allo = 3;
            }
            _b_out = _a_out.allocate(chunk_to_allo);
            _e_out = _b_out + chunk_to_allo;
            for (int i = 0; i < chunk_to_allo; ++i) {
                _b_out[i] = _a_in.allocate(chunk_size);
            }
            _b = (chunk_to_allo * chunk_size) / 3;
            _e = _b + s;
            my_uninitialized_copy(_a_in, std::begin(rhs), std::end(rhs), begin());
        }
        assert(valid());
    }

    /**
     * init-list constructor with a user given allocator type constructor
     * @param rhs the init-list to construct with
     * @param a the user given allocator
     */
    my_deque (std::initializer_list<value_type> rhs, const allocator_type& a) :
        _a_in(a),
        _a_out() {
        auto s = std::distance(std::begin(rhs), std::end(rhs));
        if (s != 0) {
            int actual_num = s / chunk_size;
            int chunk_to_allo = actual_num * 2 + 1;
            if(chunk_to_allo == 1){
                chunk_to_allo = 3;
            }
            _b_out = _a_out.allocate(chunk_to_allo);
            _e_out = _b_out + chunk_to_allo;
            for (int i = 0; i < chunk_to_allo; ++i) {
                _b_out[i] = _a_in.allocate(chunk_size);
            }
            _b = (chunk_to_allo * chunk_size) / 3;
            _e = _b + s;
            my_uninitialized_copy(_a_in, std::begin(rhs), std::end(rhs), begin());
        }
        assert(valid());
    }

    /**
     * my_deque copy constructor
     * @param that the deque to copy
     */
    my_deque (const my_deque& that) :
        _a_in(),
        _a_out() {
        auto s = that.size();
        if (s != 0) {
            int actual_num = s / chunk_size;
            int chunk_to_allo = actual_num * 2 + 1;
                        if(chunk_to_allo == 1){
                chunk_to_allo = 3;
            }
            _b_out = _a_out.allocate(chunk_to_allo);
            _e_out = _b_out + chunk_to_allo;

            for (int i = 0; i < chunk_to_allo; ++i) {
                _b_out[i] = _a_in.allocate(chunk_size);
            }
            _b = (chunk_to_allo * chunk_size) / 3;
            _e = _b + s;
            my_uninitialized_copy(_a_in, that.begin(), that.end(), begin());
        }
        assert(valid());
    }

    // ----------
    // destructor
    // ----------

    /**
     * my_deque destructor
     */
    ~my_deque () {
        // <your code>
        if (!empty()) {
            clear();
            for (size_type i = 0; i < chunk_num(); ++i) {
                _a_in.deallocate(_b_out[i], chunk_size);
            }
            _a_out.deallocate(_b_out, chunk_num());
        }

        assert(valid());
    }

    // ----------
    // operator =
    // ----------

    /**
     * assignment opeartor
     * @param rhs the right hand side deque to assign
     */
    my_deque& operator = (const my_deque& rhs) {
        if(rhs == *this){
            return *this;
        }
        resize(0);
        for (size_type i = 0; i < chunk_num(); ++i) {
            _a_in.deallocate(_b_out[i], chunk_size);
        }
        _a_out.deallocate(_b_out, chunk_num());
        _a_in = rhs._a_in;
        _a_out = rhs._a_out;
        _b_out = _a_out.allocate(rhs.chunk_num());
        for (size_type i = 0; i < rhs.chunk_num(); ++i) {
            _b_out[i] = _a_in.allocate(chunk_size);
        }
        _b = rhs._b;
        _e = rhs._e;
        my_uninitialized_copy(_a_in, std::begin(rhs), std::end(rhs), begin());
        assert(valid());
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * index opeartaor
     * @param index the index of element
     * @return the reference of indexed element
     */
    reference operator [] (size_type index) {
        assert(index >= 0 && index < size());
        return _b_out[(index + _b) / chunk_size][(index + _b) % chunk_size];
    } 

    /**
     * const index opeartaor
     * @param index the index of element
     * @return the const reference of indexed element
     */
    const_reference operator [] (size_type index) const {
        return const_cast<my_deque*>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * at method
     * @param index the index of element
     * @return the reference of indexed element
     * @throws out_of_range
     */
    reference at (size_type index) {
        if (index >= size())
            throw std::out_of_range("deque");
        return (*this)[index];
    }

    /**
     * const at method
     * @param index the index of element
     * @return the const reference of indexed element
     * @throws out_of_range
     */
    const_reference at (size_type index) const {
        return const_cast<my_deque*>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
     * back method
     * @return the reference of last element in deque
     */
    reference back () {
        // assert(0);
        return (*this)[size() - 1];
    }

    /**
     * back method
     * @return the const reference of last element in deque
     */
    const_reference back () const {
        return const_cast<my_deque*>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * begin method
     * @return the iterator pointing at first element in deque
     */
    iterator begin () {
        return iterator(this, 0);
    }

    /**
     * const begin method
     * @return the const iterator pointing at first element in deque
     */
    const_iterator begin () const {
        return const_iterator(this, 0);
    }

    // -----
    // clear
    // -----

    /**
     * clear method, delete everything in deque
     */
    void clear () {
        resize(0);
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * empty method
     * @return whether the deque is empty
     */
    bool empty () const {
        return !size();
    }

    // ---
    // end
    // ---

    /**
     * end method
     * @return the iterator pointing at the last element
     */
    iterator end () {
        return iterator(this, size());
    }

    /**
     * const end method
     * @return the const iterator pointing at the last element
     */
    const_iterator end () const {
        return const_iterator(this, size());
    }

    // -----
    // erase
    // -----

    /**
     * erase method
     * @param i the iterator pointing at the position to erase
     * @return the iterator at the erased position
     */
    iterator erase (iterator i) {
        auto ret = i;
        while(i != end() - 1) {
            *i = *(i + 1);
            ++i;
        }
        resize(size() - 1);
        assert(valid());
        return ret;
    }

    // -----
    // front
    // -----

    /**
     * front method
     * @return the reference of the first element
     */
    reference front () {
        assert(!empty());
        return (*this)[0];
    }

    /**
     * const front method
     * @return the const reference of the first element
     */
    const_reference front () const {
        return const_cast<my_deque*>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     * insert method
     * @param i the iterator pointng at the position to insert
     * @param v the value to insert
     * @return the interator pointing at the inserted position
     */
    iterator insert (iterator i, const_reference v) {
        resize(size() + 1);
        auto temp = end() - 1;
        while(temp != i) {
            *temp = *(temp - 1);
            --temp;
        }
        *i = v;
        assert(valid());
        return i;
    }

    // ---
    // pop
    // ---

    /**
     * pop_back method, delete the last element
     */
    void pop_back () {
        assert(!empty());
        resize(size() - 1);
        assert(valid());
    }

    /**
     * pop_back method, delete the first element
     */
    void pop_front () {
        assert(!empty());
        erase(begin());
        assert(valid());
    }

    // ----
    // push
    // ----

    /**
     * push_back method
     * @param v the value to push back
     */
    void push_back (const_reference v) {
        if(_e < chunk_num() * chunk_size) {
            ++_e;
            *(end() - 1) = v;

        }
        else {
            resize(size() + 1, v);
        }
        assert(valid());
    }

    /**
     * push_front method
     * @param v the value to push front
     */
    void push_front (const_reference v) {
        if(_b != 0) {
            --_b;
            (*this)[0] = v;
        }
        else {
            resize(size() + 1);
            --_b;
            --_e;
            (*this)[0] = v;
        }
        assert(valid());
    }

    // ------
    // resize
    // ------

    /**
     * resize method
     * @param s the new size
     */
    void resize (size_type s) {
        resize(s,value_type());
        assert(valid());
    }

    /**
     * resize method with fill in value
     * @param s the new size
     * @param v the value to fill in new space
     */
    void resize (size_type s, const_reference v) {
        if (s == size())
            return;
        if (s < size()) {
            my_destroy(_a_in, begin() + s, end());
            _e = _b + s;
        }
        // resize larger than capacity
        else {
            int chunk_to_alloc = std::max(s / chunk_size * 2 + 1, 3 * chunk_num());
            T** temp = _a_out.allocate(chunk_to_alloc);
            // allocate memory for first third
            for(int i = 0; i < chunk_to_alloc / 3; i++) {
                *temp = _a_in.allocate(chunk_size);
                ++temp;
            }
            // assign pointers to second third
            for(size_type i = chunk_to_alloc / 3; i < (size_type)(2 * chunk_to_alloc / 3); ++i) {
                if(i - chunk_to_alloc / 3 >= chunk_num()) {
                    *temp = _a_in.allocate(chunk_size);
                }
                else {
                    *temp = _b_out[i - chunk_to_alloc / 3];
                }
                ++temp;
            }
            // allocate memory for the last third
            for(int i = 2 * chunk_to_alloc / 3; i < chunk_to_alloc; ++i) {
                *temp = _a_in.allocate(chunk_size);
                ++temp;
            }
            _e_out = temp;
            _b_out = _e_out - chunk_to_alloc;

            int old_size = size();
            _b += (chunk_to_alloc / 3) * chunk_size;
            _e = _b + s;
            my_uninitialized_fill(_a_in, begin() + old_size, end(), v);
        }
        assert(valid());
    }


    // ----
    // size
    // ----

    /**
     * size method
     * @return size of the deque
     */
    size_type size () const {
        return _b == _e ? 0 : _e - _b;
    }

    /**
     * capacity method
     * @return capacity of the deque's container
     */
    size_type capacity() const {
        return (_e_out - _b_out) * chunk_size;
    }

    /**
     * chunk_num method
     * @return number of chunks this deque has
     */
    size_type chunk_num() const {
        return _e_out - _b_out == 0 ? 1 : _e_out - _b_out;
    }
    // ----
    // swap
    // ----

    /**
     * swap method
     * @param rhs the right hand side deque to swap with
     */
    void swap (my_deque& rhs) {
        if (_a_in == rhs._a_in) {
            std::swap(_b_out, rhs._b_out);
            std::swap(_e_out, rhs._e_out);
            std::swap(_b, rhs._b);
            std::swap(_e, rhs._e);
        }
        else {
            my_deque x(*this);
            *this = rhs;
            rhs   = x;
        }

        assert(valid());
    }
};

#endif // Deque_h
