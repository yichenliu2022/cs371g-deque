// ------------
// Vector5T.c++
// ------------

// http://en.cppreference.com/w/cpp/container/vector

#include <algorithm>        // equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=, move
#include <vector>           // vector

#include "gtest/gtest.h"

using namespace std;
using namespace testing;

using rel_ops::operator!=;
using rel_ops::operator<=;
using rel_ops::operator>;
using rel_ops::operator>=;

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);}
    return b;}

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;}}
    catch (...) {
        my_destroy(a, p, x);
        throw;}
    return x;}

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;}}
    catch (...) {
        my_destroy(a, p, b);
        throw;}}

template <typename T, typename A = allocator<T>>
class my_vector {
    friend bool operator == (const my_vector& lhs, const my_vector& rhs) {
        return (lhs.size() == rhs.size()) && equal(lhs.begin(), lhs.end(), rhs.begin());}

    friend bool operator < (const my_vector& lhs, const my_vector& rhs) {
        return lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());}

    friend void swap (my_vector& x, my_vector& y) {
        x.swap(y);}

    public:
        using allocator_type  = A;
        using value_type      = typename allocator_type::value_type;

        using size_type       = typename allocator_type::size_type;
        using difference_type = typename allocator_type::difference_type;

        using pointer         = typename allocator_type::pointer;
        using const_pointer   = typename allocator_type::const_pointer;

        using reference       = typename allocator_type::reference;
        using const_reference = typename allocator_type::const_reference;

        using iterator        = typename allocator_type::pointer;
        using const_iterator  = typename allocator_type::const_pointer;

    private:
        allocator_type _a;

        pointer _b = nullptr;
        pointer _e = nullptr; // size
        pointer _l = nullptr; // capacity

    private:
        bool valid () const {
            return (!_b && !_e && !_l) || ((_b <= _e) && (_e <= _l));}

        my_vector (const my_vector& rhs, size_type c) :
                _a (rhs._a) {
            assert(c >= rhs.size());
            _b = _a.allocate(c);
            _e = _b + rhs.size();
            _l = _b + c;
            my_uninitialized_copy(_a, rhs._b, rhs._e, _b);
            assert(valid());}

    public:
        my_vector () = default;

        explicit my_vector (size_type s) :
                _a () {
            if (s != 0) {
                _b = _a.allocate(s);
                _e = _b + s;
                _l = _e;
                my_uninitialized_fill(_a, begin(), end(), value_type());}
            assert(valid());}

        my_vector (size_type s, const_reference v) :
                _a () {
            if (s != 0) {
                _b = _a.allocate(s);
                _e = _b + s;
                _l = _e;
                my_uninitialized_fill(_a, begin(), end(), v);}
            assert(valid());}

        my_vector (size_type s, const_reference v, const allocator_type& a) :
                _a (a) {
            if (s != 0) {
                _b = _a.allocate(s);
                _e = _b + s;
                _l = _e;
                my_uninitialized_fill(_a, begin(), end(), v);}
            assert(valid());}

        my_vector (initializer_list<value_type> rhs) :
                _a () {
            if (rhs.size() != 0) {
                _b = _a.allocate(rhs.size());
                _e = _b + rhs.size();
                _l = _e;
                my_uninitialized_copy(_a, rhs.begin(), rhs.end(), begin());}
            assert(valid());}

        my_vector (initializer_list<value_type> rhs, const allocator_type& a) :
                _a (a) {
            if (rhs.size() != 0) {
                _b = _a.allocate(rhs.size());
                _e = _b + rhs.size();
                _l = _e;
                my_uninitialized_copy(_a, rhs.begin(), rhs.end(), begin());}
            assert(valid());}

        my_vector (const my_vector& rhs) :
                _a (rhs._a) {
            if (rhs.size() != 0) {
                _b = _a.allocate(rhs.size());
                _e = _b + rhs.size();
                _l = _e;
                my_uninitialized_copy(_a, rhs.begin(), rhs.end(), begin());}
            assert(valid());}

        my_vector (my_vector&& rhs) :
                _a (move(rhs._a)) {
            swap(rhs);
            assert(valid());}

        ~my_vector () {
            if (!empty()) {
                clear();
                _a.deallocate(_b, capacity());}
            assert(valid());}

        my_vector& operator = (const my_vector& rhs) {
            if (this == &rhs)
                return *this;
            if (rhs.size() == size())
                copy(rhs._b, rhs._e, _b);
            else if (rhs.size() < size()) {
                copy(rhs._b, rhs._e, _b);
                resize(rhs.size());}
            else if (rhs.size() <= capacity()) {
                copy(rhs._b, rhs._b + size(), _b);
                _e = my_uninitialized_copy(_a, rhs._b + size(), rhs._e, _e);}
            else {
                clear();
                reserve(rhs.size());
                _e = my_uninitialized_copy(_a, rhs._b, rhs._e, _b);}
            assert(valid());
            return *this;}

        my_vector& operator = (my_vector&& rhs) {
            if (this == &rhs)
                return *this;
            my_vector that(move(rhs));
            swap(that);
            assert(valid());
            return *this;}

        reference operator [] (size_type i) {
            assert(i < size());
            return _b[i];}

        const_reference operator [] (size_type i) const {
            return const_cast<my_vector&>(*this)[i];}

        reference at (size_type i) {
            if (i >= size())
                throw out_of_range("vector");
            return (*this)[i];}

        const_reference at (size_type i) const {
            return const_cast<my_vector&>(*this).at(i);}

        reference back () {
            assert(!empty());
            return *(_e - 1);}

        const_reference back () const {
            return const_cast<my_vector&>(*this).back();}

        iterator begin () {
            return _b;}

        const_iterator begin () const {
            return const_cast<my_vector&>(*this).begin();}

        size_type capacity () const {
            return (_b == nullptr) ? 0 : _l - _b;}

        void clear () {
            resize(0);
            assert(valid());}

        bool empty () const {
            return (size() == 0);}

        iterator end () {
            return _e;}

        const_iterator end () const {
            return const_cast<my_vector&>(*this).end();}

        reference front () {
            assert(!empty());
            return *_b;}

        const_reference front () const {
            return const_cast<my_vector&>(*this).front();}

        void pop_back () {
            assert(!empty());
            resize(size() - 1);
            assert(valid());}

        void push_back (const_reference v) {
            resize(size() + 1, v);
            assert(valid());}

        void reserve (size_type c) {
            if (c > capacity()) {
                my_vector x(*this, c);
                swap(x);}
            assert(valid());}

        void resize (size_type s, const_reference v = value_type()) {
            if (s == size())
                return;
            if (s < size())
                _e = my_destroy(_a, _b + s, _e);
            else if (s <= capacity()) {
                my_uninitialized_fill(_a, _e, _b + s, v);
                _e = _b + s;}
            else {
                reserve(max(2 * size(), s));
                resize(s, v);}
            assert(valid());}

        size_type size () const {
            return _b == nullptr ? 0 : _e - _b;}

        void swap (my_vector& rhs) {
            if (_a == rhs._a) {
                std::swap(_b, rhs._b);
                std::swap(_e, rhs._e);
                std::swap(_l, rhs._l);}
            else {
                my_vector x(*this);
                *this = rhs;
                rhs   = x;}
            assert(valid());}};

template <typename T>
struct VectorFixture : Test {
    using vector_type    = T;
    using allocator_type = typename vector_type::allocator_type;
    using iterator       = typename vector_type::iterator;};

using
    vector_types =
    Types<
           vector<int>,
        my_vector<int>,
           vector<int, allocator<int>>,
        my_vector<int, allocator<int>>>;

#ifdef __APPLE__
    TYPED_TEST_CASE(VectorFixture, vector_types,);
#else
    TYPED_TEST_CASE(VectorFixture, vector_types);
#endif

TYPED_TEST(VectorFixture, test0) {
    using vector_type = typename TestFixture::vector_type;
    const vector_type x;
    ASSERT_EQ(x.size(),     0u);
    ASSERT_EQ(x.capacity(), 0u);}

TYPED_TEST(VectorFixture, test1) {
    using vector_type = typename TestFixture::vector_type;
    const vector_type x(3);
    ASSERT_EQ(x.size(),     3u);
    ASSERT_EQ(x.capacity(), 3u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({0, 0, 0})));}

TYPED_TEST(VectorFixture, test2) {
    using vector_type = typename TestFixture::vector_type;
    const vector_type x(3, 2);
    ASSERT_EQ(x.size(),     3u);
    ASSERT_EQ(x.capacity(), 3u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({2, 2, 2})));}

TYPED_TEST(VectorFixture, test3) {
    using vector_type = typename TestFixture::vector_type;
    const initializer_list<int> x = {2, 3, 4};
    ASSERT_EQ(x.size(), 3u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({2, 3, 4})));
    const vector_type y = x;
    ASSERT_EQ(y.size(),     3u);
    ASSERT_EQ(y.capacity(), 3u);
    ASSERT_TRUE(equal(begin(y), end(y), begin({2, 3, 4})));}

TYPED_TEST(VectorFixture, test4) {
    using vector_type = typename TestFixture::vector_type;
    vector_type x = {2, 3, 4};
    ASSERT_EQ(x[1], 3);
    try {
        ASSERT_EQ(x.at(3), 5);
        ASSERT_TRUE(false);}
    catch (const out_of_range&)
        {}
    x[1] = 5;
    ASSERT_TRUE(equal(begin(x), end(x), begin({2, 5, 4})));}

TYPED_TEST(VectorFixture, test5) {
    using vector_type = typename TestFixture::vector_type;
    const vector_type x = {2, 3, 4};
    ASSERT_EQ(x[1], 3);
    // x[1] = 5;                                             // error: assignment of read-only location 'x.my_vector<int>::operator[](1)'
    ASSERT_TRUE(equal(begin(x), end(x), begin({2, 3, 4})));}

TYPED_TEST(VectorFixture, test6) {
    using vector_type = typename TestFixture::vector_type;
    const vector_type x(10, 2);
    const vector_type y = x;
    ASSERT_NE(&x[0], &y[0]);
    ASSERT_EQ(x,     y);}

TYPED_TEST(VectorFixture, test7) {
    using vector_type = typename TestFixture::vector_type;
    const vector_type x(10, 2);
          vector_type y(20, 3);
    ASSERT_EQ(y.size(),     20u);
    ASSERT_EQ(y.capacity(), 20u);
    y = x;
    ASSERT_EQ(y.size(),     10u);
    ASSERT_EQ(y.capacity(), 20u);
    ASSERT_EQ(y[1],         2);
    ASSERT_NE(&x[0],        &y[0]);
    ASSERT_EQ(x,            y);}

TYPED_TEST(VectorFixture, test8) {
    using vector_type = typename TestFixture::vector_type;
    const vector_type x(15, 2);
    vector_type       y(10, 3);
    ASSERT_EQ(y.size(),     10u);
    ASSERT_EQ(y.capacity(), 10u);
    y.push_back(3);
    ASSERT_EQ(y.size(),     11u);
    ASSERT_EQ(y.capacity(), 20u);
    ASSERT_NE(x,            y);
    y = x;
    ASSERT_EQ(y.size(),     15u);
    ASSERT_EQ(y.capacity(), 20u);
    ASSERT_EQ(x,            y);}

TYPED_TEST(VectorFixture, test9) {
    using vector_type = typename TestFixture::vector_type;
    const vector_type x(20, 3);
          vector_type y(10, 2);
    ASSERT_EQ(y.size(),     10u);
    ASSERT_EQ(y.capacity(), 10u);
    y = x;
    ASSERT_EQ(y.size(),     20u);
    ASSERT_EQ(y.capacity(), 20u);
    ASSERT_EQ(y[1],         3);
    ASSERT_NE(&x[0],        &y[0]);
    ASSERT_EQ(x,            y);}

TYPED_TEST(VectorFixture, test10) {
    using vector_type = typename TestFixture::vector_type;
    const vector_type x(10, 2);
    const vector_type y(10, 2);
    const vector_type z(10, 3);
    ASSERT_EQ(x, y);
    ASSERT_NE(x, z);
    ASSERT_LT(x, z);
    ASSERT_LE(x, y);
    ASSERT_GT(z, x);
    ASSERT_GE(x, y);}

TYPED_TEST(VectorFixture, test11) {
    using vector_type = typename TestFixture::vector_type;
    using iterator    = typename TestFixture::iterator;
    vector_type        x(10, 2);
    iterator           b = begin(x);
    const vector_type  y = move(x);
    ASSERT_EQ(x.size(),  0u);
    ASSERT_EQ(y.size(), 10u);
    ASSERT_EQ(b,        begin(y));}

TYPED_TEST(VectorFixture, test12) {
    using vector_type = typename TestFixture::vector_type;
    using iterator    = typename TestFixture::iterator;
    vector_type x(20, 3);
    iterator    b = begin(x);
    vector_type y(10, 2);
    y = move(x);
    ASSERT_EQ(x.size(),  0u);
    ASSERT_EQ(y.size(), 20u);
    ASSERT_EQ(b,        begin(y));}

TYPED_TEST(VectorFixture, test13) {
    using vector_type    = typename TestFixture::vector_type;
    using allocator_type = typename TestFixture::allocator_type;
    const vector_type x(3, 2, allocator_type());
    ASSERT_TRUE(equal(begin(x), end(x), begin({2, 2, 2})));}

TYPED_TEST(VectorFixture, test14) {
    using vector_type = typename TestFixture::vector_type;
    vector_type x(10);
    ASSERT_EQ(x.size(),     10u);
    ASSERT_EQ(x.capacity(), 10u);
    x.reserve(5);
    ASSERT_EQ(x.size(),     10u);
    ASSERT_EQ(x.capacity(), 10u);
    x.reserve(15);
    ASSERT_EQ(x.size(),     10u);
    ASSERT_EQ(x.capacity(), 15u);}

TYPED_TEST(VectorFixture, test15) {
    using vector_type = typename TestFixture::vector_type;
    vector_type x(10);
    ASSERT_EQ(x.size(),     10u);
    ASSERT_EQ(x.capacity(), 10u);
    x.resize(5);
    ASSERT_EQ(x.size(),      5u);
    ASSERT_EQ(x.capacity(), 10u);
    x.resize(8);
    ASSERT_EQ(x.size(),      8u);
    ASSERT_EQ(x.capacity(), 10u);
    x.resize(15);
    ASSERT_EQ(x.size(),     15u);
    ASSERT_EQ(x.capacity(), 16u);
    x.resize(50);
    ASSERT_EQ(x.size(),     50u);
    ASSERT_EQ(x.capacity(), 50u);}

TYPED_TEST(VectorFixture, test16) {
    using vector_type = typename TestFixture::vector_type;
    vector_type x;
    x.push_back(2);
    x.push_back(3);
    x.push_back(4);
    x.push_back(2);
    x.push_back(3);
    ASSERT_EQ(x.front(),    2);
    ASSERT_EQ(x.back(),     3);
    ASSERT_EQ(x.size(),     5u);
    ASSERT_EQ(x.capacity(), 8u);
    x.pop_back();
    ASSERT_EQ(x.size(),     4u);
    ASSERT_EQ(x.capacity(), 8u);}

TYPED_TEST(VectorFixture, test17) {
    using vector_type = typename TestFixture::vector_type;
    vector_type x;
    x.push_back(2);
    x.push_back(3);
    x.push_back(4);
    ASSERT_EQ(x.size(),     3u);
    ASSERT_EQ(x.capacity(), 4u);
    const vector_type y(x);
    ASSERT_EQ(y.size(),     3u);
    ASSERT_EQ(y.capacity(), 3u);
    ASSERT_EQ(x, y);}
